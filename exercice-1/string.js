
function ucfirst(string)
{
  try {
    if (string.charAt(0) === ' ') {
      return ' ' + string[1].toUpperCase() + string.substr(2).toLowerCase();
    }
    return string[0].toUpperCase() + string.substr(1).toLowerCase();
  } catch (e) {
    return '';
  }
}

function capitalize(string)
{
  try {
    return string.trim().split(' ').map(item => ucfirst(item)).join(' ');
  } catch (e) {
    return '';
  }
}

function camelCase(string) {
  try {
    return string.trim().split(' ').map(item => ucfirst(item)).join('');
  } catch(e) {
    return '';
  }
}

function snake_case(string) {
  try {
    return string.trim().split(' ').map(item => item.toLowerCase()).join('_');
  } catch(e) {
    return '';
  }
}

function leet(string) {
  let chars = {
    'a': '4',
    'e': '3',
    'i': '1',
    'o': '0',
    'u': '(_)',
    'y': '7',
  };

  try {
    return string.replace(/[aeiouy]/gi, function(char) {
      return chars[char.toLowerCase()] || char;
    });
  } catch (e) {
    return '';
  }
}

function prop_access(property, string) {
  try {
    let props = string.split('.');
    for (prop of props) {
      if (!property[prop]){
        console.log(string + " not exists");
        return;
      }
      property = property[prop];
    }
    return property;
  } catch (e) {
    return '';
  }
}

function verlan(string) {
  try {
    return string.split(' ').map(word => word.split('').reverse().join('')).join(' ');
  } catch(e) {
    return '';
  }
}

function yoda(string) {
  try {
    return string.trim().split(' ').reverse().join(" ");
  } catch (e) {
    return '';
  }
}


function vig(key, word) {
  let encryptedWord = "";
  for (let i = 0; i < word.length; i++) {
    encryptedWord[i] = (word[i] + key[i]) % 26;
  }
}
/**
 *
 * @type {*[]}
 */
let strings = [
    'test',
    'TEST',
    ' test',
    'Anaconda',
    'ANACONDA',
    'anaconda',
    'hello world !',
    'Hello world!',
    4289
];


/**
 * Test section
 */
test(ucfirst);

function test(my_function) {
  for (string of strings) {
    console.log(my_function(string));
  }
}